package com.example.challengebreakingbadapi.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.challengebreakingbadapi.repository.actor.local.dao.ActorDao
import com.example.challengebreakingbadapi.repository.actor.local.model.ActorEntity


@Database(
    entities = [
        ActorEntity::class
    ],
    version = 1,
    exportSchema = false
    )
@TypeConverters(RoomConverters::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun actorDao(): ActorDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            INSTANCE = INSTANCE ?: Room.databaseBuilder(
                context.applicationContext, AppDatabase::class.java, "ActorDatabase"
            ).build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }

}