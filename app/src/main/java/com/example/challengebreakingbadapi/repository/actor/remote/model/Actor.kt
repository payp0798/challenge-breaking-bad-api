package com.example.challengebreakingbadapi.repository.actor.remote.model

import com.example.challengebreakingbadapi.repository.actor.local.model.ActorEntity
import com.google.gson.annotations.SerializedName

data class Actor(
    @SerializedName("char_id")
    val id: Int = -1,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("birthday")
    val birthday: String = "",
    @SerializedName("occupation")
    val occupation: List<String> = listOf(),
    @SerializedName("img")
    val img: String = "",
    @SerializedName("status")
    val status: String = "",
    @SerializedName("appearance")
    var appearance: List<Int> = listOf(),
    @SerializedName("nickname")
    val nickname: String = "",
    @SerializedName("portrayed")
    val portrayed: String = "",
    var isFavorite: Boolean = false
)

fun Actor.toActorEntity():ActorEntity {
    return ActorEntity(
        this.id,
        this.name,
        this.birthday,
        this.img,
        this.status,
        this.nickname,
        this.portrayed,
        this.isFavorite
    )
}