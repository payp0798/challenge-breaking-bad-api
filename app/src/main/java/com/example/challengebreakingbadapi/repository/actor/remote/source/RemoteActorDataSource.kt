package com.example.challengebreakingbadapi.repository.actor.remote.source

import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import com.example.challengebreakingbadapi.repository.actor.remote.retrofic.ActorWebService

class RemoteActorDataSource(private val webService: ActorWebService) {

    suspend fun getAllCharacters(
        limit: Int = 10,
        offset: Int = 0,
    ): List<Actor> = webService.getAllCharacters(limit, offset)

}