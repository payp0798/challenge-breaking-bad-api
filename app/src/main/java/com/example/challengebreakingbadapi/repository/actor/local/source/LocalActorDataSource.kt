package com.example.challengebreakingbadapi.repository.actor.local.source

import com.example.challengebreakingbadapi.repository.actor.local.dao.ActorDao
import com.example.challengebreakingbadapi.repository.actor.local.model.ActorEntity
import com.example.challengebreakingbadapi.repository.actor.local.model.toActorList
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor

class LocalActorDataSource(private val actorDao: ActorDao) {

    suspend fun getAllCharacters(): List<Actor> {
        return actorDao.getAllCharacters().toActorList()
    }

    suspend fun saveActor(actorEntity: ActorEntity) {
        actorDao.saveActor(actorEntity)
    }

}