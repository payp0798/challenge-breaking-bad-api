package com.example.challengebreakingbadapi.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor

class ActorDetailViewModel: ViewModel() {

    private val actorSelected = MutableLiveData<Actor>()

    fun setActorSelected(actor: Actor) {
        this.actorSelected.value = actor
    }

    fun getActorSelected() : LiveData<Actor> {
        return this.actorSelected
    }

}