# Challenge Breaking Bad API

For the next project I will carry out an API challenge to practice and improve my skills as an Android Kotlin developer, later I will document the libraries that I will implement to improve my productivity by developing and improving my work skills

## Project Summary

For this challenge, I have decided to implement a clean architecture following the architecture model schema or map, those references will be found in the references section.
In this application several concepts were applied but I will describe each of the concepts clearly and concisely until I reach the result of the application.

As a first point I decided to make the interface design using Material Design components provided from the official google library and also, using fragments that will allow me through another library called Navigation Components to implement in a practical and fast way the navigation of fragments to through a Resource of type Navigation that we will call main_graph.xml

![image](/markdown/img1.png)

From that I have decided to start creating the MVVM architecture which is recommended by the official Android Developers site

![image](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)


Then I will start, with the repository layers in the Remote Data Source sublayer, which will have all the business logic, which will allow us to obtain the API information through the Retrofit
In fact, the Retrofit library will be very useful in this project since it will allow us to perform the server information in an adequate and practical way and will also allow us to optimize time and code savings.

After implementing the business model layer, we implement the presentation layer which will interact as an observable model object and will allow us to create the instance that will later interact with the MovieViewModel class dependency injection which

Then we implement the UI layer which will interact with the presentation layer through the injection of dependencies that we will find in the project.

One of the importance is learning to manage a local database, but for them Android offers us an official library, which will allow us to implement a structure in which we are going to create entities, then manage the information through a DAO and then create the database through the Room Database


![image](https://developer.android.com/images/training/data-storage/room_architecture.png?hl=es-419)


And finally, learning to manage the local information that will be found in a local database is important, for this in this project as a bonus I decided to add a coroutine that Android already provides us which allows us to validate the internet connection in a practical way to a mobile device, then, in the implementation we validate if the user is connected to the internet that shows the information obtained by the webservice but if the mobile device is not connected to the internet it will show the information from the database

To get an idea of the structure of the final project I will leave a screenshot of the final architecture of the project

![image](/markdown/img2.png)

As a bonus for this application, I decided to implement an infinite scroll, to understand about the behavior of adapters, the handling of the list state and how to manipulate the information correctly.

In addition, I also implemented a validation to identify when the application is connected to the internet to obtain the information from the server, through curritines, in fact this application was structured to implement coroutines, both at the level of HTTP queries, as well as queries and manipulation of the information to the database through coroutines

Later in the project demonstration you will see the resolution


## project download
```
git clone https://gitlab.com/android-kotlin3/challenge-breaking-bad-api.git
```

## Libraries to implement

```
    // 1# Navigation Components  https://developer.android.com/guide/navigation/navigation-getting-started
    implementation "androidx.navigation:navigation-fragment-ktx:2.3.5"
    implementation "androidx.navigation:navigation-ui-ktx:2.3.5"

    // 2# Retrofit https://square.github.io/retrofit/
    // Last Version https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'

    // 3# Gson https://github.com/google/gson
    implementation 'com.google.code.gson:gson:2.8.9'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'

    // 4# ViewModel https://developer.android.com/jetpack/androidx/releases/lifecycle?hl=es#declaring_dependencies
    implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.0'
    // 5# LiveData https://developer.android.com/jetpack/androidx/releases/lifecycle?hl=es#declaring_dependencies
    implementation 'androidx.lifecycle:lifecycle-livedata-ktx:2.4.0'

    // 6# Glide
    implementation 'com.github.bumptech.glide:glide:4.12.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.12.0'

    // 7# Concat Adapter https://developer.android.com/jetpack/androidx/releases/recyclerview?hl=es-419
    implementation 'androidx.recyclerview:recyclerview:1.2.1'

    // 8# Room
    implementation 'androidx.room:room-runtime:2.3.0'
    kapt 'androidx.room:room-compiler:2.3.0'

    // 9# Kotlin Extensions and Coroutines support for Room
    implementation 'androidx.room:room-ktx:2.3.0'
```



## Authors and acknowledgment
Max Herrera

## License
For open source projects, say how it is licensed.


## Solution to present:

Important detail is that the project was carried out with Kotlin for the development and management of the interface state


***
![](/markdown/demo01.gif)
***
![](/markdown/demo02.gif)
***
Through the database inspector you can see how the information is dynamically saved as the application needs to store more information

![image](/markdown/img3.png)
***




## References

- [x] [Clean Architecture Tutorial for Android: Getting Started](https://www.raywenderlich.com/3595916-clean-architecture-tutorial-for-android-getting-started)
- [x] [MVVM](https://developer.android.com/jetpack/guide)
- [x] [SOLID](https://formiik.com/publicacion/principios-solid-en-programacion)
- [x] [API The Breaking Bad](https://www.breakingbadapi.com/documentation)
- [x] [Retrofit](https://square.github.io/retrofit/)
- [x] [Navegation Components](https://developer.android.com/guide/navigation/navigation-getting-started)
- [x] [Safe Args](https://developer.android.com/guide/navigation/navigation-pass-data#kts)
- [x] [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle?hl=es#declaring_dependencies)
- [x] [Recycleview](https://developer.android.com/jetpack/androidx/releases/recyclerview?hl=es-419)
- [x] [Glide](https://github.com/bumptech/glide)
- [x] [Room](https://developer.android.com/training/data-storage/room?hl=es-419&gclsrc=aw.ds&gclid=Cj0KCQiAy4eNBhCaARIsAFDVtI3UFnnv_uejbA_s2776NV7UxAdotZwVC-R0tm0shKRxX7S4Ivjn-NMaAuyXEALw_wcB)
- [x] [Converters](https://developer.android.com/training/data-storage/room/referencing-data)
- [x] [Handling Adapters](https://stackoverflow.com/questions/31367599/how-to-update-recyclerview-adapter-data)

- [x] [Coroutines](https://developer.android.com/kotlin/coroutines?hl=es-419&gclsrc=aw.ds&gclid=Cj0KCQiA15yNBhDTARIsAGnwe0WHjGJIOPle1xvFC3-n82UxLqTuFm5WY-O2D3CLL7o6Ou-l-rdZS-waAjL6EALw_wcB)